# Markdownlint

By default the component will lint all markdown (`.md`) files in your repository.
You can override this behaviour by providing the `glob` input parameter.

To customize the linting rules, you can follow the [markdownlint-cli2 docs](https://github.com/DavidAnson/markdownlint-cli2?tab=readme-ov-file#configuration).
For example, by creating a `.markdownlint-cli2.jsonc` in the root of your repository.
Without a configuration file, the default linting rules will be used.
